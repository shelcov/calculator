# Розничный калькулятор Тома

## Настройка и запуск
Docker конфигурации содержат контейнеры:
 1. nginx
 2. postgres
 3. gunicorn

Проект содержит docker-compose файл:
 `docker-compose-local.yml` для запуска локальной конфигурации
 
Для успешного запуска необходимо указать переменные окружения в файле `.env` в корне проекта.

**Содержимое `.env` файла, для локальной сборки:**

```
ENV=.env

# Django
PYTHONUNBUFFERED=1
DJANGO_DEVELOPMENT=1
DJANGO_SETTINGS_MODULE=calculator.settings

# Database
DB_NAME=calculator
DB_USER=calculator
DB_HOST=db
DB_PASSWORD=22859788
DB_PORT=5432

# Media & staticfiles
MEDIA_URL=http://localhost/media/
STATIC_URL=/staticfiles/

```

Запуск производится в два этапа:
```
docker-compose -f docker-compose-local.yml build
docker-compose -f docker-compose-local.yml up
```

При старте gunicorn контейнера выполняется применение миграций и сбор статики 
