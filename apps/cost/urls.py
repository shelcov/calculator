from django.urls import path, include

from cost.views import CalculatorView, CostView


urlpatterns = [
    # path('ajax/', include('contents.ajax_urls')),
    path('', CalculatorView.as_view(), name='calculator'),
    path('cost/', CostView.as_view(), name='cost'),
]
