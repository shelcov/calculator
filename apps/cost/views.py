# vim:fileencoding=utf-8
from django.http import JsonResponse
from django.views import View
from django.views.generic import TemplateView

from cost.models import StateTax, Discount


class CalculatorView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        ctx = super(CalculatorView, self).get_context_data(**kwargs)
        ctx['states_tax'] = StateTax.objects.all().order_by('name')
        return ctx


class CostView(View):
    def post(self, request):
        data = request.POST.copy()
        cost = int(data.get('price')) * int(data.get('count'))
        discount = Discount.objects.filter(order_cost__lte=cost, active=True).order_by('order_cost').last()
        state_tax = StateTax.objects.filter(id=data.get('state')).first()
        percent_discount = discount.discount if discount else 0
        percent_state_tax = state_tax.tax if state_tax else 0
        cost_with_discount = cost * ((100 - percent_discount) / 100)
        cost = cost_with_discount * ((100 + percent_state_tax) / 100)
        return JsonResponse({'cost': round(cost, 3), 'state': data.get('state')})
