# vim:fileencoding=utf-8
from django.db import models


class Discount(models.Model):
    order_cost = models.IntegerField('Стоимость заказа более', default=0,
                                     help_text='Скидка будет применина, если стоимость заказа больше введенного числа')
    discount = models.IntegerField('Скидка (%)', default=0)
    active = models.BooleanField('Активна', default=True)

    class Meta:
        verbose_name = 'Скидка'
        verbose_name_plural = 'Скидки'

    def __str__(self):
        return f'Скидка {self.discount}%'


class StateTax(models.Model):
    name = models.CharField('Штат', max_length=255)
    tax = models.FloatField('Налоговая ставка (%)', default=0)

    class Meta:
        verbose_name = 'Налог штата'
        verbose_name_plural = 'Налоги штатов'

    def __str__(self):
        return self.name
