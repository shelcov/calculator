from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cost', '0001_initial'),
    ]

    def insertData(apps, schema_editor):
        Discount = apps.get_model('cost', 'Discount')
        Discount.objects.create(order_cost=1000, discount=3)
        Discount.objects.create(order_cost=5000, discount=5)
        Discount.objects.create(order_cost=7000, discount=7)
        Discount.objects.create(order_cost=10000, discount=10)
        Discount.objects.create(order_cost=50000, discount=15)

        StateTax = apps.get_model('cost', 'StateTax')
        StateTax.objects.create(name='UT', tax=6.85)
        StateTax.objects.create(name='NV', tax=8)
        StateTax.objects.create(name='TX', tax=6.25)
        StateTax.objects.create(name='AL', tax=4)
        StateTax.objects.create(name='CA', tax=8.25)

    operations = [
        migrations.RunPython(insertData),
    ]
