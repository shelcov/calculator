from django.contrib import admin

from cost.models import StateTax, Discount


class StateTaxAdmin(admin.ModelAdmin):
    list_display = ('name', 'tax')
    ordering = ('name',)


admin.site.register(Discount)
admin.site.register(StateTax, StateTaxAdmin)
