from django.test import TestCase

from cost.models import StateTax


class TestAddStateTax(TestCase):
    def setUp(self):
        self.state_tax = StateTax(
            name='UT',
            tax=6.85
        )

    def test_str(self):
        str_state_tax = str(self.state_tax)
        self.assertEqual(str_state_tax, 'UT')

    def test_type(self):
        self.assertTrue(isinstance(self.state_tax, StateTax))
