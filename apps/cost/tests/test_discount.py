from django.test import TestCase

from cost.models import Discount


class TestAddDiscount(TestCase):
    def setUp(self):
        self.discount = Discount(
            order_cost=1000,
            discount=3,
            active=True
        )

    def test_str(self):
        str_discount = str(self.discount)
        self.assertEqual(str_discount, 'Скидка 3%')

    def test_type(self):
        self.assertTrue(isinstance(self.discount, Discount))
