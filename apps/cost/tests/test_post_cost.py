from django.test import TestCase

from cost.models import Discount, StateTax


class TestPostCost(TestCase):
    def setUp(self):
        Discount.objects.create(order_cost=1000, discount=3, active=True)
        StateTax.objects.create(name='UT', tax=6.85)

    def test_success_post_cost_not_discount_not_state(self):
        data = {'count': 10, 'price': 10, 'state': 0}
        response = self.client.post('/cost/', data)
        self.assertEqual(response.json(), {'cost': 100.0, 'state': '0'})

    def test_success_post_cost_discount_not_state(self):
        data = {'count': 10, 'price': 100, 'state': 0}
        response = self.client.post('/cost/', data)
        self.assertEqual(response.json(), {'cost': 970.0, 'state': '0'})


    def test_success_post_cost_discount_and_state(self):
        data = {'count': 10, 'price': 100, 'state': 1}
        response = self.client.post('/cost/', data)
        self.assertEqual(response.json(), {'cost': 1036.445, 'state': '1'})