from django.test import TestCase
from django.urls import reverse


class TestCalculatorPage(TestCase):
    def test_calculator_context(self):
        response = self.client.get(reverse('calculator'), follow=True)
        self.assertEqual(response.status_code, 200)
